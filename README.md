# SUPL 3GPP LPP client (ROS2 package)

This ROS2 LPP client is provided as an example client for connecting to Location Server using LPP. It is a ROS2 wrapper for the [SUPL 3GPP LPP client](https://github.com/Ericsson/SUPL-3GPP-LPP-client) (v3.3.1) powered by Ericsson.

To do this the code includes three libraries: (lpp) for handling LPP messages, (rtcm_generator) for generating RTCM from LPP messages, and (modem) for getting cell information from a connected modem. The client is given as an example on how these libraries are used by them self and together and is good base for developing a full feature client. 

**MODIFICATIONS:**  
SDU UAS Center have made changes to ``lpp.h``/`lpp.cpp` to allow binding of callback to member functions of class by use of `std::function`. Changes has also been made to `rtcm_generator.cpp` to allow passing a vector pointer and get each RTCM message in seperate ``vector<unsigned char`` for split and parse.

---

## Table of Contents
- [Usage](#usage)
- [Prerequisites](#prerequisites)
- [Flight data](#flight-data)
- [License](#license)

---


## Prerequisites
The package has been tested and validated with the following hardware:
* Raspberry Pi 4 (Model B) running [Ubuntu 22.04.3 LTS (Jammy Jellyfish)](https://releases.ubuntu.com/jammy/)
* [ROS2 Humble](https://docs.ros.org/en/humble/index.html)

These are the prerequisites required for building: 
```bash
sudo apt install g++ cmake libssl-dev ninja-build
```



## Usage
The ROS2 LPP client node can be run with the following `ros2 run` command:

```
ros2 run rtk_lpp_client rtk_lpp_client --ros-args \
-p ls_host:=<IP> \
-p modem_dev:=<DEVICE> \
-p ls_port:=<INT> \
-p ls_ssl:=<BOOL> \
-p cell_mcc:=<INT> \
-p cell_mnc:=<INT> \
-p cell_tac:=<INT> \
-p cell_cell:=<INT> \
-p msm_type:=<INT>
```

## Flight data
The available flight data has been conducted with the following hardware:
* [Holybro Pixhawk 6C Mini](https://docs.px4.io/v1.14/en/flight_controller/pixhawk6c_mini.html) w/ [PX4 v.1.14](https://github.com/PX4/PX4-Autopilot/releases)
* [Holybro H-RTK F9P GNSS](https://docs.px4.io/v1.14/en/gps_compass/rtk_gps_holybro_h-rtk-f9p.html)
* [SIMCom SIM8202E](https://en.simcom.com/product/SIM8202X_M2.html) with 2x [Taoglas FXUB71](https://www.taoglas.com/product/fxub71-ultra-wide-band-flex-2-mimo/)


See [flight data](flight_data/) folder for; flight logs with corresponding collected measurements of: 
* RTK GNSS: **[HDOP, VDOP, Horizontal accuracy, Vertical accuracy]**
* 5G NSA: LTE **[RSSI, RSRP, RSRQ, SNR]** and 5G **[RSRP, RSRQ, SNR]**

## License
See [LICENSE](LICENSE.txt) File.

