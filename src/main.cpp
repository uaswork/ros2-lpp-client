#include <rtk_lpp_client.hpp>

int  main(int argc, char** argv) {
     // Launch ROS 2
    rclcpp::init(argc, argv);

    // Make the node spin
    auto node = std::make_shared<rtcm_lpp_client>();

     // setup modem and LPP client
    if (!node->setupLppClient()) {
        printf("Setup of LPP client failed... Exiting");
        std::cout << std::endl;
        return 0;
    }

    // run node, ROS and LPP client process loop
    node->run(node);
    
    // Shutdown routine for ROS2
    node->shutdown();

    return 0;

}

