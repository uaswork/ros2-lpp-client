#include "rtk_lpp_client.hpp"

rtcm_lpp_client::rtcm_lpp_client() : Node("LPP_CLIENT"){
    // Constructor
    
    // initialize ros
    
    // read parameters and save internally for use in modem setup
    this->declare_parameter("ls_host", "");
    this->declare_parameter("modem_dev","/dev/null");
    this->declare_parameter("ls_port", 0);
    this->declare_parameter("ls_ssl", false);

    ls_host = this->get_parameter("ls_host").get_parameter_value().get<std::string>();
    modem_dev = this->get_parameter("modem_dev").get_parameter_value().get<std::string>();
    ls_port = this->get_parameter("ls_port").get_parameter_value().get<int>();
    ls_ssl = this->get_parameter("ls_ssl").get_parameter_value().get<bool>();

    this->declare_parameter("cell_mcc", 0);
    this->declare_parameter("cell_mnc", 0);
    this->declare_parameter("cell_tac", 0);
    this->declare_parameter("cell_cell", 0);
    this->declare_parameter("msm_type", 7);

    int _mcc = this->get_parameter("cell_mcc").get_parameter_value().get<int>();
    int _mnc = this->get_parameter("cell_mnc").get_parameter_value().get<int>();
    int _tac = this->get_parameter("cell_tac").get_parameter_value().get<int>();
    int _cell = this->get_parameter("cell_cell").get_parameter_value().get<int>();

    // int msm_select = this->get_parameter("msm_type").get_parameter_value().get<int>();

    // save cellID setup
    cell = CellID {
        .mcc  = _mcc,
        .mnc  = _mnc,
        .tac  = _tac,
        .cell = _cell,
    };

    // #QoS define for publisher
    auto qos = rclcpp::QoS(rclcpp::KeepLast(1)).reliable().durability_volatile();

    // Create publisher for the corrections messages
    ros_rtcm_publisher = this->create_publisher<mavros_msgs::msg::RTCM>("gps_rtk/send_rtcm", qos);
}

int rtcm_lpp_client::setupLppClient() {

    // modem options
    //auto modem_dev = _modem_dev;
    auto modem_baud = 9600;

    printf("\n \nmodem_dev: %s \n \n", modem_dev.c_str());

    printf("Cell:            MCC=%ld, MNC=%ld, TAC=%ld, Id=%ld\n", cell.mcc, cell.mnc, cell.tac, cell.cell);

    // Initialize OpenSSL
    network_initialize();

    // Register callback for when the location server request location
    // information from the device.
    client.provide_location_information_callback(NULL, std::bind(&rtcm_lpp_client::provide_location_information_callback, this, std::placeholders::_1, std::placeholders::_2));

    // Register callback for when the location server request ECID and
    // measurements from the device.
    client.provide_ecid_callback(NULL, std::bind(&rtcm_lpp_client::provide_ecid_callback, this, std::placeholders::_1, std::placeholders::_2));

    // Connect to location server
    if (!client.connect(ls_host, ls_port, ls_ssl, cell)) {
        printf("ERROR: Connection failed. (%s:%i%s)\n", ls_host.c_str(), ls_port,
               ls_ssl ? " [ssl]" : "");
        return 0;
    }

    // Initialize Modem
    modem = new Modem_AT(modem_dev.c_str(), modem_baud, cell);
    if (!modem->initialize()) {
        printf("ERROR: Modem connection failed. (device: %s, baud-rate: %u)\n", modem_dev.c_str(),
               modem_baud);
        return 0;
    }

    // Enable generation of message for GPS, GLONASS, Galileo, and Beidou.
    auto gnss = GNSSSystems{
        .gps     = true,
        .glonass = true,
        .galileo = true,
        .beidou  = true,
    };

    printf("  gnss support:   ");
    if (gnss.gps) printf(" GPS");
    if (gnss.glonass) printf(" GLO");
    if (gnss.galileo) printf(" GAL");
    if (gnss.beidou) printf(" BDS");
    printf("\n");

    int msm_select = this->get_parameter("msm_type").get_parameter_value().get<int>();
    printf("\nmsm_select: %i \n", msm_select);

    // Filter what messages you want generated. (MSM6 is not supported)
    auto filter = MessageFilter{};
    if (msm_select == 4) {
        filter.msm.msm4 = true;
        filter.msm.msm5 = false;
        filter.msm.msm7 = false;
    } else if (msm_select == 5) {
        filter.msm.msm4 = false;
        filter.msm.msm5 = true;
        filter.msm.msm7 = false;
    } else if (msm_select == 7) {
        filter.msm.msm4 = false;
        filter.msm.msm5 = false;
        filter.msm.msm7 = true;
    }

    printf("  msm support:    ");
    if (filter.msm.msm4) printf(" MSM4");
    if (filter.msm.msm5) printf(" MSM5");
    if (filter.msm.msm7) printf(" MSM7");
    printf("\n");

    printf("MSM Messages:   ");
    if (filter.msm.msm4) printf(" MSM4");
    if (filter.msm.msm5) printf(" MSM5");
    if (filter.msm.msm7) printf(" MSM7");
    printf("\n");

    // Create RTCM generator for converting LPP messages to RTCM messages.
    generator = RTCMGenerator{gnss, filter};

    // Request assistance data from server for 'cell' and register a callback
    // when we receive assistance data.
    #if !SSR
        auto request = client.request_assistance_data(cell, NULL, std::bind(&rtcm_lpp_client::assistance_data_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    #else
        //auto request = client.request_assistance_data_ssr(cell, NULL, assistance_data_callback);
        auto request = client.request_assistance_data_ssr(cell, NULL, std::bind(&rtcm_lpp_client::assistance_data_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
   #endif
        if (request == AD_REQUEST_INVALID) {
            printf("ERROR: Requesting assistance data failed.\n");
            return 0;
        }
    return 1;
}

void rtcm_lpp_client::run(rclcpp::Node::SharedPtr _node) {
    // forever loop - ROS, runs the program, 100ms
    rclcpp::Rate loop_rate(10);
    while (rclcpp::ok()) {
    
        rclcpp::spin_some(_node);

        // client.process() MUST be called at least once every second.
        if (!client.process()) {
            printf("ERROR: Client has lost connection.\n");
            return;
        }

        loop_rate.sleep();
        std::this_thread::sleep_for(std::chrono::milliseconds(25));;
    }
}

void rtcm_lpp_client::shutdown() {
    network_cleanup();
}

bool rtcm_lpp_client::provide_location_information_callback(LocationInformation* location,
                                                            UNUSED void* userdata) {
    location->time = time(NULL);
    location->lat  = 20;
    location->lon  = 20;
    return true;
}

bool rtcm_lpp_client::provide_ecid_callback(ECIDInformation* ecid, UNUSED void* userdata) {
    if (!modem) return false;

    auto neighbors = modem->neighbor_cells();
    auto cell = modem->cell();
    if (!cell.initialized()) return false;

    ecid->cell           = cell.value();
    ecid->neighbor_count = 0;

    for (auto& neighbor_cell : neighbors) {
        if (ecid->neighbor_count < 16) {
            auto& nb  = ecid->neighbors[ecid->neighbor_count++];
            nb.id     = neighbor_cell.id;
            nb.earfcn = neighbor_cell.EARFCN;
            nb.rsrp   = neighbor_cell.rsrp;
            nb.rsrq   = neighbor_cell.rsrq;
        }
    }

    return true;
}

void rtcm_lpp_client::assistance_data_callback(LPP_Client*, LPP_Transaction*,
                                               LPP_Message* message, void*) {
#if !SSR
    // Our vector object of messages
    std::vector<std::vector<unsigned char>> messageList;
    
    if (!generator.process(message)) {
        // Segmented LPP message, waiting for rest before converting to RTCM.
        return;
    }
    
    // Convert LPP message to buffer of RTCM coded messages.
    Generated     generated_messages{};
    unsigned char buffer[4 * 4096];
    auto          size   = sizeof(buffer);
    auto          length = generator.convert(buffer, &size, message, &generated_messages, &messageList);

    printf("length: %4li | msm%i | ", length, generated_messages.msm);
    if (generated_messages.mt1074) printf("1074 ");
    if (generated_messages.mt1075) printf("1075 ");
    if (generated_messages.mt1076) printf("1076 ");
    if (generated_messages.mt1077) printf("1077 ");
    if (generated_messages.mt1084) printf("1084 ");
    if (generated_messages.mt1085) printf("1085 ");
    if (generated_messages.mt1086) printf("1086 ");
    if (generated_messages.mt1087) printf("1087 ");
    if (generated_messages.mt1094) printf("1094 ");
    if (generated_messages.mt1095) printf("1095 ");
    if (generated_messages.mt1096) printf("1096 ");
    if (generated_messages.mt1097) printf("1097 ");
    if (generated_messages.mt1030) printf("1030 ");
    if (generated_messages.mt1031) printf("1031 ");
    if (generated_messages.mt1230) printf("1230 ");
    if (generated_messages.mt1006) printf("1006 ");
    if (generated_messages.mt1032) printf("1032 ");
    printf("\n");

    // send as ros message
    if (!messageList.empty()) {
        for (auto item : messageList) {
            mavros_msgs::msg::RTCM ros_msg;
            ros_msg.header.stamp = this->get_clock()->now();
            // ros_msg.header.stamp = now();
            ros_msg.data = item;
            // printf("data: %p\n", item);
            ros_rtcm_publisher->publish(ros_msg);
        }
    }
#else
    // Extract SSR data
    printf("SSR LPP Message: %p\n", (void*)message);
#endif

}

void rtcm_lpp_client::split_and_send_rtcm_ros(vector<unsigned char> &msg) {
    // split the message if needed, pack it into mavlink frames and send them in ros to mavlink_bridge if the length of message is bigger than we can have in 4 messages over mavlink, we can't send it

    // printf("Data: %p\n", msg);

    if (msg.size() > 180 * 4) {
        printf("RTCM Message too large, skipping");
    }

    // find number of messages required
    int msgs_num = 0;

    // divide by message length, if overflow we add extra package
    if (msg.size() % 180 == 0)
        msgs_num = std::floor(msg.size() / 180);
    else
        msgs_num = std::floor(msg.size() / 180) + 1;

    // make messages
    for (auto i = 0; i < msgs_num; i++) {
        // flag
        unsigned char flags = 0;

        // set fragmentation flag, if sending more than 1 message
        if (msgs_num > 1)
            flags = 1;

        // set fragmentation id
        flags |= (i & 0x3) << 1;

        // set id overall of sequence
        flags |= (rtcm_sequence_num & 0x1f) << 3;

        // get data amount, must be 180, or less depending what fragmentation we are at
        auto data_amount = std::min(int(msg.size()) - (i * 180), 180);
        // chunk of data to send. From fragmentation id + amount
        vector<unsigned char> data_chunk;
        data_chunk.assign(msg.begin() + (i * 180), msg.begin() + (i * 180 + data_amount));

        // fill up the data array
        data_chunk.resize(180, '\0');

        mavros_msgs::msg::RTCM ros_msg;
        // printf("Data: %p\n", data_chunk);

        ros_msg.header.stamp = now();
        ros_msg.data  = data_chunk;
        ros_rtcm_publisher->publish(ros_msg);
    }
    
    // send 0-byte terminal message if we send exactly 2 or 3 full msgs.
    //  Otherwise it might think more is coming as we don't have a closing or 4th message
    if (msgs_num < 4 && (msg.size() % 180 == 0) && msg.size() > 180) {
        // transmit 0-byte termination message
        unsigned char flags = 1;
        flags |= (msgs_num & 0x3) << 1;
        flags |= (rtcm_sequence_num & 0x1f) << 3;
        
        mavros_msgs::msg::RTCM ros_msg;

        ros_msg.header.stamp = now();
        ros_msg.data  = {};
        ros_rtcm_publisher->publish(ros_msg);
    }

    // increment sequence id
    rtcm_sequence_num++;
}