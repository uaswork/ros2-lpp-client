#ifndef RTCM_LPP_CLIENT_RTCM_LPP_CLIENT_HPP
#define RTCM_LPP_CLIENT_RTCM_LPP_CLIENT_HPP

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include <mavros_msgs/msg/rtcm.hpp>

#include <lpp.h>
#include <modem.h>
#include <rtcm_generator.h>
#include <vector>
#include <functional>

#include <thread>

#include <cmath> 
#include <iostream>

#include <typeinfo>
#include <iterator>


//#define UNUSED [[maybe_unused]]
#define SSR 0

using namespace std;

class rtcm_lpp_client : public rclcpp::Node {

public:
    rtcm_lpp_client();

    int  setupLppClient();
    void run(rclcpp::Node::SharedPtr _node);
    void shutdown();

private:
    // ROS pubs/subs
    rclcpp::Publisher<mavros_msgs::msg::RTCM>::SharedPtr ros_rtcm_publisher;

    // sequence vars
    unsigned int rtcm_sequence_num = 0;

    // functions
    void split_and_send_rtcm_ros(vector<unsigned char> &msg);

    // LPP vars
    RTCMGenerator generator;
    LPP_Client    client{false};
    CellID        cell{};
    Modem_AT*     modem{};

    // Config vars
    std::string ls_host;
    std::string modem_dev;
    int ls_port;
    bool ls_ssl;
    int msm_select;
    int msm_type;

    // LPP Functions
    bool provide_location_information_callback(LocationInformation* location, void* userdata);
    bool provide_ecid_callback(ECIDInformation* ecid, void* userdata);
    // void assistance_data_callback(LPP_Client*, LPP_Transaction* transaction, LPP_Message* message, void* userdata);
    void assistance_data_callback(LPP_Client*, LPP_Transaction* transaction, LPP_Message* message, void* userdata);
};

#endif  // RTCM_LPP_CLIENT_RTCM_LPP_CLIENT_HPP
